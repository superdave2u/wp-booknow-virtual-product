<?php
/*
Plugin Name: WooCommerce BookNow Virtual Products
Description: Use this plugin to change the "Add to Cart" button text to "Enroll Now"
*/
class WooCommerceBookProducts {
	function __construct() {
		add_filter( 'single_add_to_cart_text'	, array($this, 'woo_custom_cart_button_text') );
	 	add_filter( 'variable_add_to_cart_text'	, array($this, 'woo_custom_cart_button_text') );
	}
	function woo_custom_cart_button_text() {
		return __('Book Now', 'woocommerce');
    } 
}
$WooCommerceBookProducts = new WooCommerceBookProducts();
?>
