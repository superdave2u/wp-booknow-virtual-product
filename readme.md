/*
Plugin Name: WooCommerce BookNow Virtual Products
Description: Use this plugin to change the "Add to Cart" button text to "Enroll Now"
*/